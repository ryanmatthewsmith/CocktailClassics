//
//  Cocktail.swift
//  CocktailClassics
//
//  Created by Ryan Matthew Smith on 8/8/17.
//  Copyright © 2017 Ryan Matthew Smith. All rights reserved.
//
//  Cocktail struct

import Foundation
import UIKit

struct Cocktail{
   
   enum Served{
      case up
      case rocks
      case crushed
   }
   
   enum Chill{
      case doubleShake
      case shake
      case stir
   }
   
   enum Equipment{
      case barspoon
      case coneStrainer
      case jiggerSet
      case juicer
      case julepStrainer
      case hawthorneStrainer
      case knife
      case mixingGlass
      case muddler
      case shaker
      case toothpick
      case vegetablePeeler
   }
   
   enum Glass{
      case martini
      case highball
      case rocks
      case julep
      case coupe
      case flute
      
   }
   
   enum Flavor{
      case refreshing
      case rich
      case sweet
      case tangy
      case bitter
   }
   
   enum Spirit{
      case whiskey
      case gin
      case tequila
      case cognac
      case rum
      case pisco
      case bitters
   }
   
   enum Garnish{
      case bitters
      case lemonPeel
      case limeWedge
      case limeSlice
      case limeWheel
      case limePeel
      case luxardo
      case mint
      case olive
      case orangePeel
      case saltRim
      case varies
   }
   
   //fonts
   private let headlineFont = UIFont(name: "Avenir-Medium", size: 16.0)
   private let copyFont = UIFont(name: "Avenir-Light", size: 16.0)
   
   private let _name: String
   private let _chill: Chill
   private let _baseSpirit: Spirit
   private let _served: Served
   private let _glass: Glass
   private let _ingredients: [(ingredient: String, quantity: Double)]
   private let _garnish: [Garnish]
   private let _steps: String
   private let _flavorProfile: Dictionary<Flavor, Int>
   private let _mainFlavor: Flavor
   private let _equipment: [Equipment]
   private var _oz: Bool = true
   
   
   
   //--GETTERS--//
   var name: String{
      get{
         return _name
      }
   }
   
   var chill: Chill{
      get{
         return _chill
      }
   }
   
   var served: Served{
      get{
         return _served
      }
   }
   
   var baseSpirit: Spirit{
      get{
         return _baseSpirit
      }
   }
   
   var glass: Glass{
      get{
         return _glass
      }
   }
   
   var ingredients: [(String, Double)]{
      get{
         return _ingredients
      }
   }
   
   var garnish: [Garnish]{
      get{
         return _garnish
      }
   }
   
   var steps: String{
      get{
         return _steps + "\n"
      }
   }
   
   var flavorProfile: Dictionary<Flavor, Int>{
      get{
         return _flavorProfile
      }
   }
   
   var mainFlavor: Flavor{
      get{
         return _mainFlavor
      }
   }
   
   //--Initialize Struct--//
   
   init(name: String, chill: Chill,
        served: Served, glass: Glass, baseSpirit: Spirit,
        ingredients: [(ingredient: String, quantity: Double)],
        steps: String, flavorProfile: Dictionary<Flavor, Int>,
        garnish: [Garnish], equipment: [Equipment]
      ){
      
      self._name = name
      self._chill = chill
      self._served = served
      self._glass = glass
      self._baseSpirit = baseSpirit
      self._ingredients = ingredients
      self._steps = steps
      self._flavorProfile = flavorProfile
      self._equipment = equipment
      
      var biggestFlavorValue = 0
      var biggestFlavor: Flavor = Flavor.rich
      for element in flavorProfile{
         if element.value > biggestFlavorValue{
            biggestFlavorValue = element.value
            biggestFlavor = element.key
         }
      }
      
      self._mainFlavor = biggestFlavor
      
      //garnish can be empty
      self._garnish = garnish
   }
   
   func getCocktailGlassAsString() -> String{
      switch _glass {
      case .coupe: return "Chilled Coupe Glass"
      case .flute: return "Chilled Champagne Flute"
      case .highball: return "Highball Glass"
      case .julep: return "Julep Cup"
      case .martini: return "Chilled Martini Glass"
      case .rocks: return "Rocks Glass"
      }
   }
   
   func getEquipmentString() -> String{
      
      var equipmentListString = getCocktailGlassAsString() + "\n"
      for equip in _equipment{
         equipmentListString += getEquipmentStringSingle(equip) + "\n"
      }
      return equipmentListString
   }
   
   func getEquipmentStringSingle(_ equip: Equipment) -> String{
      switch equip{
      case .barspoon: return "Barspoon"
      case .coneStrainer: return "Fine Mesh Cone Strainer"
      case .hawthorneStrainer: return "Hawthorne Strainer"
      case .jiggerSet: return "Jigger Set"
      case .juicer: return "Citrus Juicer"
      case .julepStrainer: return "Julep or Hawthorne Strainer"
      case .knife: return "Knife"
      case .mixingGlass: return "Cocktail Mixing Glass"
      case .muddler: return "Muddler"
      case .shaker: return "Boston Shaker"
      case .toothpick: return "Toothpick"
      case .vegetablePeeler: return "Swiss Peeler"
      }
   }
   
   func getIngredientString() -> String{
      var ingredientAsString = ""
      for (ingredient, quantity) in _ingredients {
         ingredientAsString += ingredient
         if quantity > 0.0 {
            ingredientAsString += ", " + String(quantity) + " " + (_oz ? "oz" : "ml")
         }
         ingredientAsString += "\n"
      }

      return ingredientAsString + "\n"
   }
   
   func getFormattedAttributedString(_ headlineString: String,
                                     _ copyString: String,
                                     _ headlineColor: UIColor) -> NSMutableAttributedString{
      
      let headline = headlineString + "\n"
      //let copyString = copyString //_cocktail.getIngredientString()
      
      let fullString = headline + copyString
      let attributedText = NSMutableAttributedString(string: fullString)
      
      let headlineRange = (fullString as NSString).range(of: headline)
      let copyStringRange = (fullString as NSString).range(of: copyString)
      
      attributedText.addAttribute(NSFontAttributeName, value: headlineFont!, range: headlineRange)
      attributedText.addAttribute(NSForegroundColorAttributeName, value: headlineColor, range: headlineRange)
      attributedText.addAttribute(NSFontAttributeName, value: copyFont!, range: copyStringRange)
      attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: copyStringRange)
      
      return attributedText

   }
   
   func getGarnishString() -> String{
      var garnishAsString = ""
      
      if _garnish.count == 0{ return ""}
      
      for gar in _garnish{
         garnishAsString += getGarnishString(gar) + "\n"
      }
      return garnishAsString
   }
   
   private func getGarnishString(_ garnish: Garnish) -> String{
      switch garnish{
      case .bitters: return "Angostura Bitters, 2 Dashes"
      case .lemonPeel: return "Lemon Peel"
      case .limePeel: return "Lime Peel"
      case .limeSlice: return "Lime Wheel"
      case .limeWedge: return "Lime Wedge"
      case .luxardo: return "Brandied Cherry"
      case .mint: return "Mint Bouquet"
      case .orangePeel: return "Orange Peel"
      case .olive: return "Olive"
      case .saltRim: return "Salted Rim"
      case .varies: return "Varies by Martini Style, see below"
      default: return "Update Garnish Switch"
      }
      
   }
   
   

}
