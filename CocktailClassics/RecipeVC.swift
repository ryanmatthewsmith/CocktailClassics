//
//  RecipeVC.swift
//  CocktailClassics
//
//  Created by Ryan Matthew Smith on 8/14/17.
//  Copyright © 2017 Ryan Matthew Smith. All rights reserved.
//

import UIKit

class RecipeVC: UIViewController {

   @IBOutlet weak var cocktailImage: UIImageView!
   
   @IBOutlet weak var IngredientsField: UITextView!
   @IBOutlet weak var PropertiesButtonStackView: UIStackView!
   @IBOutlet weak var DirectionsField: UITextView!
   
   @IBOutlet weak var ButtonGlass: UIButton!
   @IBOutlet weak var ButtonChill: UIButton!
   @IBOutlet weak var ButtonServe: UIButton!
   
   @IBOutlet weak var EquipmentField: UITextView!
   
   @IBOutlet weak var scrollingHeightConstraint: NSLayoutConstraint!
   let bottomPadding: CGFloat = -180
   
   var _cocktail: Cocktail!
   var _hasGarnish = false
   
   var cocktail: Cocktail{
      get{
         return _cocktail
      } set{
         _cocktail = newValue
      }
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      changeMainImage()
      editTextFields()
      setButtonImages()
      
//      //size scrolling window
//      var size = cocktailImage.frame.size.height
//      print(size)
//      size += ButtonChill.frame.size.height
//      print(size)
//      size += IngredientsField.frame.size.height
//      print(size)
//      size += DirectionsField.frame.size.height
//      print(size)
//      size += EquipmentField.frame.size.height
//      print(size)
//      size += bottomPadding
      
      //self.scrollingHeightConstraint.constant = size
      
      self.view?.setNeedsDisplay()
      
      //size scrolling window
      var size = cocktailImage.frame.size.height
      size += ButtonChill.frame.size.height
      size += IngredientsField.frame.size.height
      size += DirectionsField.frame.size.height
      size += EquipmentField.frame.size.height
      size += bottomPadding
      
      self.scrollingHeightConstraint.constant = size
      self.view?.setNeedsDisplay()
   }
   
   func changeMainImage(){
      let cocktailMainImageName = _cocktail.name.replacingOccurrences(of: " ", with: "")
      cocktailImage.image = UIImage(named: cocktailMainImageName)
   }
   
   func setButtonImages(){
      //names of proper buttons
      let buttonGlassName = "Glass_" + String(describing: _cocktail.glass)
      let buttonChillName = "Chill_" + String(describing: _cocktail.chill)
      let buttonServeName = "Serve_" + String(describing: _cocktail.served)
      
      //Glass button
      if let buttonGlassImage = UIImage.init(named: buttonGlassName){
         ButtonGlass.setImage(buttonGlassImage, for: .normal)
         ButtonGlass.imageView?.contentMode = .scaleAspectFit
         ButtonGlass.imageView?.layer.borderWidth = 0.5
         ButtonGlass.imageView?.layer.borderColor = UIColor.lightGray.cgColor
      }
      
      //Chill button
      if let buttonChillImage = UIImage.init(named: buttonChillName){
         ButtonChill.setImage(buttonChillImage, for: .normal)
         ButtonChill.imageView?.contentMode = .scaleAspectFit
         ButtonChill.imageView?.layer.borderWidth = 0.5
         ButtonChill.imageView?.layer.borderColor = UIColor.lightGray.cgColor
      }
      
      //Serve button
      if let buttonServeImage = UIImage.init(named: buttonServeName){
         ButtonServe.setImage(buttonServeImage, for: .normal)
         ButtonServe.imageView?.contentMode = .scaleAspectFit
         ButtonServe.imageView?.layer.borderWidth = 0.5
         ButtonServe.imageView?.layer.borderColor = UIColor.lightGray.cgColor
      }
   }
   
   func editTextFields(){
      
      let cocktailColor: UIColor = ModelCocktails.init().spiritColors[_cocktail.baseSpirit]!
      
      //margins
      IngredientsField.textContainerInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
      DirectionsField.textContainerInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
      EquipmentField.textContainerInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
      
      
      // Formatted ingredient and optional garnish string
      let ingredientString = _cocktail.getFormattedAttributedString("INGREDIENTS",
                                                                             _cocktail.getIngredientString(),
                                                                             cocktailColor)
      
      if _cocktail.garnish.count > 0{
         let garnishString = _cocktail.getFormattedAttributedString("GARNISH",
                                                                                _cocktail.getGarnishString(),
                                                                                cocktailColor)
         ingredientString.append(garnishString)
      }
      
      IngredientsField.attributedText = ingredientString
      adjustUITextViewHeight(text: IngredientsField)
      
      // Formatted Directions String
      let directionsString = _cocktail.getFormattedAttributedString("DIRECTIONS",
                                                                    _cocktail.steps,
                                                                    cocktailColor)
      DirectionsField.attributedText = directionsString
      adjustUITextViewHeight(text: DirectionsField)
      
      // Formatted Equipment String
      let equipmentString = _cocktail.getFormattedAttributedString("EQUIPMENT", _cocktail.getEquipmentString(), cocktailColor)
      EquipmentField.attributedText = equipmentString
      adjustUITextViewHeight(text: EquipmentField)
   }
   
   func adjustUITextViewHeight(text : UITextView)
   {
      text.sizeToFit()
   }

//   override func didReceiveMemoryWarning() {
//      super.didReceiveMemoryWarning()
//      // Dispose of any resources that can be recreated.
//   }
   
   @IBAction func dimissViewController(_ sender: Any) {
      dismiss(animated: true, completion: nil)
   }
   @IBAction func dismissViewController(_ sender: UIBarButtonItem) {
      dismiss(animated: true, completion: nil)
   }
   

   /*
   // MARK: - Navigation
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
   }
   */

}
