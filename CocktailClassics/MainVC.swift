//
//  ViewController.swift
//  CocktailClassics
//
//  Created by Ryan Matthew Smith on 8/8/17.
//  Copyright © 2017 Ryan Matthew Smith. All rights reserved.
//

import UIKit

class MainVC: UIViewController{
   
   private var cocktailModel: ModelCocktails?
   private var cocktailsArray: [Cocktail] = [Cocktail]()
   private var cocktailIndex: Int?

   //row 1, cols 1-5
   @IBOutlet weak var button1: UIButton!
   @IBOutlet weak var button2: UIButton!
   @IBOutlet weak var button3: UIButton!
   @IBOutlet weak var button4: UIButton!
   @IBOutlet weak var button5: UIButton!
   //row 2
   @IBOutlet weak var button6: UIButton!
   @IBOutlet weak var button7: UIButton!
   @IBOutlet weak var button8: UIButton!
   @IBOutlet weak var button9: UIButton!
   @IBOutlet weak var button10: UIButton!
   //row 3
   @IBOutlet weak var button11: UIButton!
   @IBOutlet weak var button12: UIButton!
   @IBOutlet weak var button13: UIButton!
   @IBOutlet weak var button14: UIButton!
   @IBOutlet weak var button15: UIButton!
   //row 4
   @IBOutlet weak var button16: UIButton!
   @IBOutlet weak var button17: UIButton!
   @IBOutlet weak var button18: UIButton!
   @IBOutlet weak var button19: UIButton!
   @IBOutlet weak var button20: UIButton!
   //row 5
   @IBOutlet weak var button21: UIButton!
   @IBOutlet weak var button22: UIButton!
   @IBOutlet weak var button23: UIButton!
   @IBOutlet weak var button24: UIButton!
   @IBOutlet weak var button25: UIButton!
   
   @IBOutlet weak var recipeButton: UIButton!
   @IBOutlet weak var mainImage: UIImageView!
   var buttonCallers: [UIButton] = [UIButton]()

   override func viewDidLoad() {
      super.viewDidLoad()
      
      cocktailIndex = 0
      cocktailModel = ModelCocktails.init()
      cocktailsArray = (cocktailModel?.cocktails)!
      self.buttonCallers = [button1, button2, button3, button4, button5,
                            button6, button7, button8, button9, button10,
                            button11, button12, button13, button14, button15,
                            button16, button17, button18, button19, button20,
                            button21, button22, button23, button24, button25]
      
      //set buttonsUp
      setButtonImages(cocktailsArray)
      changeMainImage(cocktailsArray[cocktailIndex!])
      changeRecipeButtonColor(cocktailsArray[cocktailIndex!])
   }
   @IBAction func leftGesture(_ sender: UISwipeGestureRecognizer) {
      //perform segue to recipe view
      if let index = cocktailIndex{
         performSegue(withIdentifier: "RecipeVC", sender: cocktailsArray[index])
      }
   }

   @IBAction func viewRecipe(_ sender: UIButton) {
      //perform segue to recipe view
      if let index = cocktailIndex{
         performSegue(withIdentifier: "RecipeVC", sender: cocktailsArray[index])
      }
   }
   
   @IBAction func recipeSelected(_ sender: UIButton) {
      
      cocktailIndex = sender.tag
      //change image to last selected
      let currentSelectedCocktail = cocktailsArray[cocktailIndex!]
      changeMainImage(currentSelectedCocktail)
      changeRecipeButtonColor(currentSelectedCocktail)
      
      //redraw
      self.view?.setNeedsDisplay()
   }
   
   func changeMainImage(_ selectedCocktail: Cocktail){

      let cocktailMainImageName = selectedCocktail.name.replacingOccurrences(of: " ", with: "")
      mainImage.image = UIImage(named: cocktailMainImageName)
      
      //redraw
      self.view?.setNeedsDisplay()
   }
   
   func changeRecipeButtonColor(_ selectedCocktail: Cocktail){
      
      //In case of coloring by flavor
      //recipeButton.layer.borderColor = cocktailModel?.flavorProfileColors[selectedCocktail.mainFlavor]?.cgColor
      //recipeButton.setTitleColor(cocktailModel?.flavorProfileColors[selectedCocktail.mainFlavor], for: .normal)
      //recipeButton.titleLabel?.textColor = cocktailModel?.flavorProfileColors[selectedCocktail.mainFlavor]
      
      recipeButton.layer.borderColor = cocktailModel?.spiritColors[selectedCocktail.baseSpirit]?.cgColor
      recipeButton.layer.borderWidth = 1
      recipeButton.setTitleColor(cocktailModel?.spiritColors[selectedCocktail.baseSpirit], for: .normal)
      recipeButton.titleLabel?.textColor = cocktailModel?.spiritColors[selectedCocktail.baseSpirit]
   }
   
   func setButtonImages(_ cocktails: [Cocktail]){
      //set buttonsUp
      let numberOfCocktails = cocktails.count
      for index in 0...(numberOfCocktails - 1){
         let imageName = cocktails[index].name.replacingOccurrences(of: " ", with: "") + "_Button"
         if let nextButtonImage = UIImage.init(named: imageName){
            buttonCallers[index].setImage(nextButtonImage, for: .normal)
            buttonCallers[index].imageView?.contentMode = .scaleAspectFit
            
            //border
            buttonCallers[index].imageView?.layer.borderWidth = 0.5
            buttonCallers[index].imageView?.layer.borderColor = UIColor.lightGray.cgColor
            //buttonCallers[index].layer.borderWidth = 0.5
            //buttonCallers[index].layer.borderColor = UIColor.lightGray.cgColor
            
         }
      }
   }
   
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if let destination = segue.destination as? RecipeVC{
         if let passCocktail = sender as? Cocktail{
            destination.cocktail = passCocktail
         }
      }
   }
   
//   override func didReceiveMemoryWarning() {
//      super.didReceiveMemoryWarning()
//      // Dispose of any resources that can be recreated.
//   }

}

