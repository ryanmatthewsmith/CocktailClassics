//
//  Model.swift
//  CocktailClassics
//
//  Created by Ryan Matthew Smith on 8/8/17.
//  Copyright © 2017 Ryan Matthew Smith. All rights reserved.
//
//  Small form app, all data 

import Foundation
import UIKit

struct ModelCocktails{
   
   private var _cocktails: [Cocktail]
   
   //Flavor Colors
   private let _spiritColors: [Cocktail.Spirit : UIColor]
   
   var cocktails: [Cocktail]{
      get{
         return _cocktails
      }
   }
   
   var spiritColors: [Cocktail.Spirit : UIColor]{
      get{
         return _spiritColors
      }
   }
   
   //-- Aviation --//
   private var aviation = Cocktail(name: "Aviation", chill: .shake, served: .up,
                                   glass: .coupe, baseSpirit: .gin,
                                   ingredients: [("Gin" , 2.0),
                                                 ("Lemon Juice" , 0.75),
                                                 ("Luxardo Maraschino Liqeuer" , 0.5),
                                                 ("1:1 Simple Syrup" , 0.25),
                                                 ("Creme de Violette, 0.5 tsp" , 0.0)],
                                   steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.  Garnish with brandied cherry.",
                                   flavorProfile: [.refreshing : 3,
                                                   .rich : 0,
                                                   .sweet: 4,
                                                   .tangy: 2,
                                                   .bitter: 0],
                                   garnish: [.luxardo],
                                   equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer])
   
   //-- Boulevardier --//
   private var boulevardier = Cocktail(name: "Boulevardier", chill: .stir, served: .up,
                                       glass: .coupe, baseSpirit: .whiskey,
                                       ingredients: [("Bourbon Whiskey" , 1.5),
                                                     ("Campari" , 0.75),
                                                     ("Sweet Vermouth" , 0.75)],
                                       steps: "Stir ingredients over ice.  Strain into a chilled coupe glass.  Rim and garnish with lemon peel.",
                                       flavorProfile: [.refreshing : 0,
                                                       .rich : 4,
                                                       .sweet: 2,
                                                       .tangy: 0,
                                                       .bitter: 3],
                                       garnish: [.lemonPeel],
                                       equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler])
   
   //--Brooklyn--//
   private var brooklyn = Cocktail(name: "Brooklyn", chill: .stir, served: .up,
                                       glass: .coupe, baseSpirit: .whiskey,
                                       ingredients: [("Rye Whiskey" , 2.0),
                                                     ("Dry Vermouth" , 0.75),
                                                     ("Amaro" , 0.25),
                                                     ("Luxardo Maraschino Liqueur, 1 tsp" , 0.0)],
                                       steps: "Stir ingredients over ice.  Strain into chilled coupe glass.",
                                       flavorProfile: [.refreshing : 0,
                                                       .rich : 4,
                                                       .sweet: 0,
                                                       .tangy: 0,
                                                       .bitter: 4],
                                       garnish: [],
                                       equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer])
   
   //-- Corpse Reviver #2 --//
   private var corpsereviver = Cocktail(name: "Corpse Reviver #2", chill: .shake, served: .up,
                                        glass: .martini, baseSpirit: .gin,
                                        ingredients: [("Gin" , 0.75),
                                                      ("Cointreau" , 0.75),
                                                      ("Lillet Blonde" , 0.75),
                                                      ("Lemon Juice" , 0.75),
                                                      ("Absinthe, 1 dash" , 0.0)],
                                        steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.",
                                        flavorProfile: [.refreshing : 1,
                                                        .rich : 1,
                                                        .sweet: 0,
                                                        .tangy: 0,
                                                        .bitter: 0],
                                        garnish: [],
                                        equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer])
   
   //-- Daquiri --//
   private var daiquiri = Cocktail(name: "Daiquiri", chill: .shake, served: .up,
                                   glass: .martini, baseSpirit: .rum,
                                   ingredients: [("Flor De Cana Rum" , 2.0),
                                                 ("Lime Juice" , 1.0),
                                                 ("1:1 Simple Syrup" , 0.75)],
                                   steps: "Shake ingredients with ice.  Double strain into a chilled coupe glass.  Garnish with lime wheel.",
                                   flavorProfile: [.refreshing : 5,
                                                   .rich : 0,
                                                   .sweet: 2,
                                                   .tangy: 2,
                                                   .bitter: 0],
                                   garnish: [.limeSlice],
                                   equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .knife])
   
   //-- French 75 --//
   private var french75 = Cocktail(name: "French 75", chill: .shake, served: .up,
                                   glass: .flute, baseSpirit: .gin,
                                   ingredients: [("Dry Gin" , 1.5),
                                                 ("Lemon Juice" , 0.75),
                                                 ("1:1 Simple Syrup" , 0.75),
                                                 ("Dry Champagne, Top" , 0.0)],
                                   steps: "Shake ingredients (except champagne) with ice.  Double strain into champagne flute.  Top glass with champagne.  Garnish with lemon twist.",
                                   flavorProfile: [.refreshing : 3,
                                                   .rich : 0,
                                                   .sweet: 2,
                                                   .tangy: 2,
                                                   .bitter: 0],
                                   garnish: [.lemonPeel],
                                   equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .vegetablePeeler])
   
   //-- Daquiri, Hemingway --//
   private var hemingway = Cocktail(name: "Hemingway Daiquiri", chill: .shake, served: .up,
                                    glass: .martini, baseSpirit: .rum,
                                    ingredients: [("Light Rum" , 2.0),
                                                  ("Grapefruit Juice" , 0.75),
                                                  ("Lime Juice" , 0.5),
                                                  ("1:1 Simple Syrup" , 0.25),
                                                  ("Luxardo Maraschino Liqueur" , 0.25)],
                                    steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.  Garnish with lime wheel.",
                                    flavorProfile: [.refreshing : 4,
                                                    .rich : 0,
                                                    .sweet: 3,
                                                    .tangy: 2,
                                                    .bitter: 0],
                                    garnish: [.limeSlice],
                                    equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .knife])
   
   //-- Last Word --//
   private var lastword = Cocktail(name: "Last Word", chill: .shake, served: .up,
                                   glass: .coupe, baseSpirit: .gin,
                                   ingredients: [("Gin" , 1.0),
                                                 ("Green Chartreuse" , 0.75),
                                                 ("Luxardo" , 0.75),
                                                 ("Lime Juice" , 0.75)],
                                   steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.",
                                   flavorProfile: [.refreshing : 3,
                                                   .rich : 0,
                                                   .sweet: 2,
                                                   .tangy: 2,
                                                   .bitter: 1],
                                   garnish: [],
                                   equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer])
   
   //-- Manhattan --//
   private var manhattan = Cocktail(name: "Manhattan", chill: .stir, served: .up,
                                    glass: .martini, baseSpirit: .whiskey,
                                    ingredients: [("Rye Whiskey" , 2.5),
                                                  ("Carpano Antica Vermouth" , 1.25),
                                                  ("Angustura, 2 dashes" , 0.0)],
                                    steps: "Stir ingredients over ice.  Strain into a chilled martini glass.  Garnish with brandied cherry or rim and garnish orange peel.  Or both.",
                                    flavorProfile: [.refreshing : 0,
                                                    .rich : 4,
                                                    .sweet: 2,
                                                    .tangy: 0,
                                                    .bitter: 2],
                                    garnish: [.orangePeel, .luxardo],
                                    equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler, .toothpick])
   
   //-- Margarita --//
   private var margarita = Cocktail(name: "Margarita", chill: .shake, served: .rocks,
                                    glass: .rocks, baseSpirit: .tequila,
                                    ingredients: [("Blanco or Reposado Tequila" , 2.0),
                                                  ("Lime Juice" , 1.0),
                                                  ("Cointreau" , 0.75),
                                                  ("1:1 Simple Syrup" , 0.5)],
                                    steps: "Salt rim of rocks glass.  Shake ingredients with ice, strain into glass containing large ice cubes.  Garnish with a small lime wedge.",
                                    flavorProfile: [.refreshing : 3,
                                                    .rich : 0,
                                                    .sweet: 2,
                                                    .tangy: 4,
                                                    .bitter: 0],
                                    garnish: [.limeSlice, .saltRim],
                                    equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .knife])
  
   //-- Martinez --//
   private var martinez = Cocktail(name: "Martinez", chill: .stir, served: .up,
                                   glass: .coupe, baseSpirit: .gin,
                                   ingredients: [("Old Tom Gin" , 2.0),
                                                 ("Carpano Antica Vermouth" , 1.25),
                                                 ("Luxardo Maraschino Liqueur" , 0.25),
                                                 ("Orange Bitters, 2 dashes" , 0.0)],
                                   steps: "Stir ingredients over ice. Strain into a chilled coupe glass.  Rim and garnish with lemon peel.",
                                   flavorProfile: [.refreshing : 0,
                                                   .rich : 3,
                                                   .sweet: 1,
                                                   .tangy: 0,
                                                   .bitter: 0],
                                   garnish: [.lemonPeel],
                                   equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler])
   
   //-- Martini, Classic --//
   private var martini = Cocktail(name: "Martini", chill: .stir, served: .up,
                                  glass: .martini, baseSpirit: .gin,
                                  ingredients: [("Dry Gin" , 2.5),
                                                ("Dry Vermouth" , 0.75)],
                                  steps: "Stir ingredients over ice.  Strain into chilled martini glass.  For Classic Martini, add 2 dashes orange bitters and garnish with lemon twist.  For Dirty Martini, add a dash of olive brine and garnish with olive.  For Gibson, add a dash of cocktail onion brine and garnish with 2 cocktail onions.",
                                  flavorProfile: [.refreshing : 0,
                                                  .rich : 2,
                                                  .sweet: 0,
                                                  .tangy: 0,
                                                  .bitter: 1],
                                  garnish: [.varies],
                                  equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler, .toothpick])
   
   //-- Mint Julep --//
   private var mintjulep = Cocktail(name: "Mint Julep", chill: .stir, served: .crushed,
                                glass: .julep, baseSpirit: .whiskey,
                                ingredients: [("Mint Leaves, 5" , 0.0),
                                              ("1:1 Simple Syrup" , 0.5),
                                              ("Bourbon" , 2.5)],
                                steps: "Gently muddle mint with simple syrup in julep cup.  Add bourbon and some crushed ice to cup and stir.  Add straw to cup, top with crushed ice, and garnish with mint bouquet.",
                                flavorProfile: [.refreshing : 0,
                                                .rich : 3,
                                                .sweet: 1,
                                                .tangy: 0,
                                                .bitter: 0],
                                garnish: [.mint],
                                equipment: [.muddler, .barspoon])
   
   //-- Mojito --//
   private var mojito = Cocktail(name: "Mojito", chill: .shake, served: .crushed,
                                 glass: .rocks, baseSpirit: .rum,
                                 ingredients: [("Mint Leaves, 5" , 0.0),
                                               ("1:1 Simple Syrup" , 0.75),
                                               ("Light Rum" , 2.0),
                                               ("Angostura Bitters, 1 dash" , 0.0)],
                                 steps: "Gently muddle mint with simple syrup in shaker.  Add remaining ingredients and shake with ice.  Strain into rocks glass with large ice cubes.  Add straw and garnish with mint bouquet.",
                                 flavorProfile: [.refreshing : 4,
                                                .rich : 0,
                                                .sweet: 0,
                                                .tangy: 0,
                                                .bitter: 0],
                                 garnish: [.mint],
                                 equipment: [.juicer, .muddler, .jiggerSet, .shaker, .hawthorneStrainer])
   
   //-- Negroni --//
   private var negroni = Cocktail(name: "Negroni", chill: .stir, served: .up,
                                  glass: .martini, baseSpirit: .gin,
                                  ingredients: [("Gin" , 1),
                                                ("Campari" , 1),
                                                ("Sweet Vermouth" , 1)],
                                  steps: "Stir ingredients over ice.  Strain into a chilled coupe glass.  Rim and garnish with orange peel.",
                                  flavorProfile: [.refreshing : 0,
                                                  .rich : 4,
                                                  .sweet: 2,
                                                  .tangy: 0,
                                                  .bitter: 3],
                                  garnish: [.orangePeel],
                                  equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler])
   
   //-- Old Cuban --//
   private var oldcuban = Cocktail(name: "Old Cuban", chill: .shake, served: .up,
                                   glass: .coupe, baseSpirit: .rum,
                                   ingredients: [("Mint Leaves, 6" , 0.0),
                                                 ("Amber Rum" , 1.5),
                                                 ("1:1 Simple Syrup" , 1.0),
                                                 ("Lime Juice" , 0.75),
                                                 ("Angostura Bitters, 2 dashes" , 0.0),
                                                 ("Chilled Dry Champagne" , 2.0)],
                                   steps: "By Audrey Sanders.  Gently muddle mint with simple syrup in shaker.  Add rum, lime juice, and bitters to shaker, then shake over ice.  Double strain into a chilled coupe glass and top with chilled champagne.",
                                   flavorProfile: [.refreshing : 3,
                                                   .rich : 3,
                                                   .sweet: 3,
                                                   .tangy: 1,
                                                   .bitter: 0],
                                   garnish: [.mint],
                                   equipment: [.juicer, .muddler, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer])
   
   //-- Old Fashioned --//
   private var oldfashioned = Cocktail(name: "Old Fashioned", chill: .stir, served: .rocks,
                                       glass: .rocks, baseSpirit: .whiskey,
                                       ingredients: [("Rye Whiskey" , 2.5),
                                                     ("Sugar Cube, 1" , 0.0),
                                                     ("Angostura Bitters, 2 dashes" , 0.0),
                                                     ("Orange Bitters, 1 dash" , 0.0)],
                                       steps: "Stir ingredients over ice.  Strain into rocks glass containing one large ice cube.  Rim and garnish with orange peel.",
                                       flavorProfile: [.refreshing : 0,
                                                       .rich : 5,
                                                       .sweet: 2,
                                                       .tangy: 0,
                                                       .bitter: 1],
                                       garnish: [.orangePeel],
                                       equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler])
   
   //-- Paloma --//
   private var paloma = Cocktail(name: "Paloma", chill: .shake, served: .rocks,
                                 glass: .highball, baseSpirit: .tequila,
                                 ingredients: [("1/2 lime" , 0.0),
                                               ("Tequila, Blanco" , 2.0),
                                               ("Grapefruit Juice" , 0.5),
                                               ("1:1 Simple Syrup" , 0.5),
                                               ("Grapefruit Soda, top" , 0.0)],
                                 steps: "Salt rim of a highball glass and add large ice cubes.  Squeeze lime and drop into shaker.  Shake with remaining ingredients (without grapefruit soda) with ice.  Strain into glass and top with grapefruit soda.  Garnish with lime slice.",
                                 flavorProfile: [.refreshing : 4,
                                                 .rich : 0,
                                                 .sweet: 2,
                                                 .tangy: 2,
                                                 .bitter: 0],
                                 garnish: [.limeWheel],
                                 equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .knife])
   
   //-- Pegu Club --//
   private var peguclub = Cocktail(name: "Pegu Club", chill: .shake, served: .up,
                                   glass: .coupe, baseSpirit: .gin,
                                   ingredients: [("Gin" , 2.0),
                                                 ("Curacao" , 0.75),
                                                 ("Lime Juice" , 0.5),
                                                 ("Angostura Bitters, 1 dash" , 0.0),
                                                 ("Orange Bitters, 1 dash" , 0.0)],
                                   steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.  Garnish with lime peel.",
                                   flavorProfile: [.refreshing : 2,
                                                   .rich : 0,
                                                   .sweet: 1,
                                                   .tangy: 1,
                                                   .bitter: 0],
                                   garnish: [.limePeel],
                                   equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .vegetablePeeler])
   
   //-- Pisco Sour --//
   private var piscosour = Cocktail(name: "Pisco Sour", chill: .doubleShake, served: .up,
                                    glass: .coupe, baseSpirit: .pisco,
                                    ingredients: [("Pisco" , 2),
                                                  ("Lime Juice" , 1.0),
                                                  ("1:1 Simple Syrup" , 0.5),
                                                  ("Egg White, 1" , 0.0)],
                                    steps: "Dry shake ingredients to build a starter foam, then shake over ice.  Double strain into chilled coupe glass.  Dash bitters on foam and spread around with a toothpick for garnish.",
                                    flavorProfile: [.refreshing : 2,
                                                    .rich : 0,
                                                    .sweet: 1,
                                                    .tangy: 3,
                                                    .bitter: 0],
                                    garnish: [.bitters],
                                    equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .toothpick])
   
   //-- Ramos Gin Fizz --//
   private var ramos = Cocktail(name: "Ramos Gin Fizz", chill: .doubleShake, served: .up,
                                glass: .martini, baseSpirit: .gin,
                                ingredients: [("Dry Gin" , 2.0),
                                              ("Heavy Cream" , 1.0),
                                              ("1:1 Simple Syrup" , 1.0),
                                              ("Lime Juice" , 0.5),
                                              ("Lemon Juice" , 0.5),
                                              ("Egg White, 1" , 0.0),
                                              ("Orange Flower Water, 4 drops" , 0.0),
                                              ("Club Soda" , 0.0)],
                                steps: "Dry shake ingredients (without club soda) to build a starter foam, then shake vigariously over ice.  Double strain into a highball or large fizz glass.  Let sit for roughly 30 seconds, then slowly top glass with club soda.  Garnish with orange twist.",
                                flavorProfile: [.refreshing : 3,
                                                .rich : 3,
                                                .sweet: 3,
                                                .tangy: 1,
                                                .bitter: 0],
                                garnish: [.orangePeel],
                                equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .vegetablePeeler])
   
   //-- Sidecar --//
   private var sidecar = Cocktail(name: "Sidecar", chill: .shake, served: .up,
                                  glass: .coupe, baseSpirit: .cognac,
                                  ingredients: [("Cognac" , 1.5),
                                                ("Cointreau" , 0.75),
                                                ("Lemon Juice" , 0.75)],
                                  steps: "Shake ingredients with ice.  Double strain into chilled coupe glass.  Rim and garnish with orange peel.",
                                  flavorProfile: [.refreshing : 3,
                                                  .rich : 1,
                                                  .sweet: 0,
                                                  .tangy: 1,
                                                  .bitter: 0],
                                  garnish: [.orangePeel],
                                  equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .vegetablePeeler])
   
   //-- Trinidad Sour --//
   private var trinidadsour = Cocktail(name: "Trinidad Sour", chill: .shake, served: .up,
                                       glass: .martini, baseSpirit: .bitters,
                                       ingredients: [("Rye Whiskey" , 1.0),
                                                     ("Angostura Bitters" , 1.0),
                                                     ("Falernum" , 0.75),
                                                     ("Lemon Juice" , 0.5)],
                                       steps: "By Giuseppe Gonzalez.  Shake ingredients with ice.  Double strain into chilled coupe glass.  ",
                                       flavorProfile: [.refreshing : 0,
                                                       .rich : 4,
                                                       .sweet: 2,
                                                       .tangy: 3,
                                                       .bitter: 5],
                                       garnish: [],
                                       equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer])
   
   //-- Whiskey Sour --//
   private var whiskeysour = Cocktail(name: "Whiskey Sour", chill: .doubleShake, served: .up,
                                      glass: .coupe, baseSpirit: .whiskey,
                                      ingredients: [("Bourbon Whiskey" , 2),
                                                    ("Lemon Juice" , 0.75),
                                                    ("1:1 Simple Syrup" , 0.75),
                                                    ("Bitters, 2 dashes" , 0.0),
                                                    ("Egg White, 1" , 0.0)],
                                      steps: "Dry shake ingredients to build a starter foam, then shake over ice.  Double strain into a chilled coupe glass.  Dash bitters on foam and spread around with a toothpick for garnish.",
                                      flavorProfile: [.refreshing : 2,
                                                      .rich : 1,
                                                      .sweet : 2,
                                                      .tangy : 2,
                                                      .bitter: 0],
                                      garnish: [.bitters],
                                      equipment: [.juicer, .jiggerSet, .shaker, .hawthorneStrainer, .coneStrainer, .toothpick])
   
   //-- Vieux Carre --//
   private var vieuxcarre = Cocktail(name: "Vieux Carre", chill: .stir, served: .rocks,
                                     glass: .rocks, baseSpirit: .cognac,
                                     ingredients: [("Rye Whiskey" , 1.0),
                                                   ("Cognac" , 1.0),
                                                   ("Sweet Vermouth" , 1.0),
                                                   ("Benedictine, 1 tsp" , 0.0),
                                                   ("Peychaud Bitters, 1 dash" , 0.0),
                                                   ("Angostura Bitters, 1 dash" , 0.0)],
                                     steps: "Stir ingredients over ice.  Strain into rocks glass containing one large ice cube.  Rim and garnish with lemon peel.",
                                     flavorProfile: [.refreshing : 0,
                                                     .rich : 5,
                                                     .sweet: 0,
                                                     .tangy: 0,
                                                     .bitter: 2],
                                     garnish: [.lemonPeel],
                                     equipment: [.jiggerSet, .mixingGlass, .barspoon, .julepStrainer, .vegetablePeeler])
   
   //-- Init object --//
   init(){
      _cocktails = [aviation, boulevardier, brooklyn, corpsereviver, daiquiri,
                    french75, hemingway, lastword, manhattan, margarita,
                    martinez, martini, mintjulep, mojito, negroni,
                    oldcuban, oldfashioned, paloma, peguclub, piscosour,
                    ramos, sidecar, trinidadsour, whiskeysour, vieuxcarre]
      _spiritColors = [.gin : UIColor.init(red: CGFloat(0.0),
                                           green: CGFloat(0.847), //216
                                           blue: CGFloat(1.0),
                                           alpha: CGFloat(1.0)),
                       .whiskey : UIColor.init(red: CGFloat(1.0),
                                               green: CGFloat(0.518), //132
                                               blue: CGFloat(0.0),
                                               alpha: CGFloat(1.0)),
                       .tequila : UIColor.init(red: CGFloat(0.478), //122
                                               green: CGFloat(1.0),
                                               blue: CGFloat(0.161), //41
                                               alpha: CGFloat(1.0)),
                       .rum : UIColor.init(red: CGFloat(1.0),
                                           green: CGFloat(0.184), //47
                                           blue: CGFloat(0.529), //135
                                           alpha: CGFloat(1.0)),
                       .bitters : UIColor.init(red: CGFloat(0.549),  //140
                                             green: CGFloat(0.263), //67
                                             blue: CGFloat(0.933), //238
                                             alpha: CGFloat(1.0)),
                       .pisco : UIColor.init(red: CGFloat(0.0),
                                             green: CGFloat(0.271), //69
                                             blue: CGFloat(0.522), //133
                                             alpha: CGFloat(1.0)),
                       .cognac : UIColor.init(red: CGFloat(1.0),
                                              green: CGFloat(0.286), //73
                                              blue: CGFloat(0.039), //10
                                              alpha: CGFloat(1.0))]
   }

}
